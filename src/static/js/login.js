// la incarcarea paginii
$(document).ready(function() {
    // se adauga evenimente de tip enter pe casutele text necesare autentificarii si inregistrarii
    $('#parola').on('keypress',function(e) {
        if(e.which == 13) { // enter
            log();
        }
    });
    $('#signparola').on('keypress',function(e) {
        if(e.which == 13) { // enter
            if (document.getElementById("signin").disabled == false)
                sign();
        }
    });
    $('#confirmare').on('keypress',function(e) {
        if(e.which == 13) { // enter
            if (document.getElementById("signin").disabled == false)
                sign();
        }
    });
});

function checkPassword()    // functie pentru verificarea integritatii parolei la inregistrare
{                           // parola trebuie sa se potriveasca cu confirmarea si sa aiba dimensiunea mai mare de 6
    var p, cp, un;
    p = document.getElementById("signparola");
    cp = document.getElementById("confirmare");
    un = document.getElementById("signusername");
    if (un.value == p.value)
    {
        document.getElementById("confirmtip").style.display = "block";
        document.getElementById("confirmtip").innerHTML = "Numele de utilizator si parola nu pot avea aceeasi valoare!";
    }
    else
    {
        if (p.value != cp.value)
        {
            document.getElementById("confirmtip").style.display = "block";
            document.getElementById("confirmtip").innerHTML = "Parolele nu se potrivesc!";
        }
        else
        {
            if (p.value.length < 6)
            {
                document.getElementById("confirmtip").innerHTML = "Parola trebuie să aibă mai mult de 6 caractere!";
                document.getElementById("confirmtip").style.display = "block";
            }
            else
            {
                document.getElementById("confirmtip").style.display = "none";
                document.getElementById("signin").disabled = false;
            }
        }
    }
}

function log()          // functie pentru logarea unui utilizator
{
    $.post('/log', { user: document.getElementById("username").value,
                     pass: document.getElementById("parola").value},
        function(result) {
            if (result == 'ok')     // daca procesul a reusit se redirectioneaza catre pagina principala
                location.href = '/';
            else                    // altfel apare o alerta cu eroarea
            {
                alert(result);
            }
        }
    );
}

function sign()         // functie pentru inregistrarea unui nou utilizator
{
    $.post('/signin', { user: document.getElementById("signusername").value,
                        pass: document.getElementById("signparola").value},
        function(result) {
            if (result == 'ok')         // rezultat ok
                location.href = '/';    // redirectionare catre pagina principala
            else                        // alfel apare o alerta cu eroarea aparuta
            {
                alert(result);
            }
        }
    );
}

