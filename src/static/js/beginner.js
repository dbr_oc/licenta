$( document ).ready(function() {    // la incarcarea paginii
    // se seteaza dimensiunile tagului cu problema
    var w = $('#pv').width();
    $('.btw').css('margin-left', w);
    $('.btw').css('margin-right', w);
    loadBlockly();      // se incarca mediul blockly
});
var workspace;              // variabila workspace trebuie sa fie globala
function loadBlockly()      // functie pentru incarcarea mediului blockly
{
    $.get("/toolbox", function(data, status){   // http get pentru toolbox
        if (status == "success")
        {
            // configurarea workspaceului
            var blocklyArea = document.getElementById('blockly-area');
            var blocklyDiv = document.getElementById('blockly-div');
            var arearez = document.getElementById('arearezultat');
            workspace = Blockly.inject(blocklyDiv, {toolbox: data});
            workspace.addChangeListener(translateFunction);
            var onresize = function(e) {    // definirea unei functii de redimensionare
                // setarea dimensiunilor conform dimensiunii noi
                var w = $('#pv').width();
                $('.btw').css('margin-left', w);
                $('.btw').css('margin-right', w);
                var element = blocklyArea;
                var x = 0;
                var y = 0;
                do {
                    x += element.offsetLeft;
                    y += element.offsetTop;
                    element = element.offsetParent;
                } while (element);
                blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
                blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
                arearez.style.height = blocklyArea.offsetHeight - 46 + 'px';
                Blockly.svgResize(workspace);
            };
            // se adauga eveniment la redimensionare
            window.addEventListener('resize', onresize, false);
            onresize();
            Blockly.svgResize(workspace);
        }
    });
}

function translateFunction()     // functie pentru translatarea blocurilor
{
    // se foloseste de functionalitatile bibliotecii blockly
    // traducerea se face in functie de valoare selectata in elementul #rezultatsel
    var sel = document.getElementById("rezultatsel");
    var code;
    if (sel.value == "py")
    {
        code = Blockly.Python.workspaceToCode(workspace);
    }
    else
    {
        code = Blockly.JavaScript.workspaceToCode(workspace);
    }
    // rezultatul este afisat in casuta text #arearezultat
    document.getElementById('arearezultat').value = code;
}
