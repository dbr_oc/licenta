//Blockly.inject('blockly-div', {
//    media: 'static/blockly/media/',
//    toolbox: document.getElementById('toolbox'),
//    toolboxPosition: 'left',
//    horizontalLayout: false,
//    scrollbars: false
//});
$( document ).ready(function() {
    loadBlockly();
});
var workspace;
function loadBlockly()
{
    $.get("/toolbox", function(data, status){
        if (status == "success")
        {
            var blocklyArea = document.getElementById('blockly-area');
            var blocklyDiv = document.getElementById('blockly-div');
            var arearez = document.getElementById('arearezultat');
            workspace = Blockly.inject(blocklyDiv, {toolbox: data});
            workspace.addChangeListener(myUpdateFunction);
            var onresize = function(e) {
                var element = blocklyArea;
                var x = 0;
                var y = 0;
                do {
                    x += element.offsetLeft;
                    y += element.offsetTop;
                    element = element.offsetParent;
                } while (element);
                // Position blocklyDiv over blocklyArea.
                blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
                blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
                arearez.style.height = blocklyArea.offsetHeight - 46 + 'px';
                Blockly.svgResize(workspace);
            };
            window.addEventListener('resize', onresize, false);
            onresize();
            Blockly.svgResize(workspace);
        }
    });
}
function myUpdateFunction(event) {
    var code = Blockly.Python.workspaceToCode(workspace);
    document.getElementById('arearezultat').value = code;
}
