$(document).ready(function() {      // la incarcarea paginii
    // se adauga un eveniment ca la introducerea de text, casuta text specifica inputului sa se redimensioneze
    $('#areasursa').bind('input', function() {
        var other = document.getElementById("arearezultat");
        var clicked = document.getElementById("areasursa");
        clicked.style.height = "1px";
        if ((25+clicked.scrollHeight) < 200)     // in cazul implicit, aceasta ramane la o dimensiune de 200px
        {
            clicked.style.height = "200px";
        }
        else
        {
            clicked.style.height = (25+clicked.scrollHeight)+"px";
        }
        other.style.height = clicked.style.height;
    });

    // in cazul in care pagina este specifica traducerii de pseudocod, trebuie afisat butonul de configurare de variabile
    if (document.getElementById("sursasel").innerHTML == "PSEUDOCOD")
    {
        document.getElementById("butonModal").style.display = "block";
        document.getElementById("rezultatsel").style.marginTop = "48px";
    }

    // se adauga un eventiment ca la apasarea tastei tab sa se introduca 4 spatii, si nu trecerea la urmatorul element
    $(document).delegate('#areasursa', 'keydown', function(e) {
        var keyCode = e.keyCode || e.which;
        // La apasarea tastei tab
        if (keyCode == 9) {
            e.preventDefault();
            var start = this.selectionStart;
            var end = this.selectionEnd;
            $(this).val($(this).val().substring(0, start) +  "\xa0\xa0\xa0\xa0" + $(this).val().substring(end));
            // reasezare
            this.selectionStart = this.selectionEnd = start + 4;
        }
    });

    // seteaza marginea pentru continutul din carousel
    var w = $('#pv').width();
    $('.btw').css('margin-left', w);
    $('.btw').css('margin-right', w);

    // se seteaza o functie pentru redimensionarea ferestrei
    $( window ).resize(function() {

        var w = $('#pv').width();
        $('.btw').css('margin-left', w);
        $('.btw').css('margin-right', w);

        // se verifica de asemenea ca de la o anumita dimensiune, portiunile in care se afiseaza textul sa fie aliniate
        // in caz contrar, acestea sunt una sub alta si nu trebuie o aliniere specifica
        if (window.innerWidth < 768)
        {
            document.getElementById("rezultatsel").style.marginTop = "inherit";
        }
        else if (document.getElementById("sursasel").innerHTML == "PSEUDOCOD")
        {
            document.getElementById("rezultatsel").style.marginTop = "48px";
        }
    });
});

var adaugate = 0;

function get_rezultate(caller)      // functie pentru obtinerea tipurilor de translari posibile, la schimbarea valorii
{
    // trebuie si aici setat ca la schimbarea valorii in pseudocod, sa se afiseze butonul de configurare de variabile
    if (caller.value != 'PSEUDOCOD')
    {
        document.getElementById("butonModal").style.display = "none";
        document.getElementById("rezultatsel").style.marginTop = "inherit";
    }
    else
    {
        document.getElementById("butonModal").style.display = "block";
        document.getElementById("rezultatsel").style.marginTop = "48px";
    }
    // se apeleaza o resursa ce ofera optiunile de traducere
    $.get("/rezultate/"+caller.value, function(data, status){
        if (status == "success")
        {
           var optiuni = this.responseText.split(',');
            var h = "";
            for (let o of optiuni)
            {
                h += '<option value="'+o+'">'+o+'</option>';
            }
            document.getElementById("rezultatsel").innerHTML = h;
        }
    });
}

function translateInput()       // functie care realizeaza apelul serviciului de traducere
{
    var input = document.getElementById("areasursa").value;
    var flang = document.getElementById("sursasel").innerHTML;
    var tolang = document.getElementById("rezultatsel").innerHTML;
    var names = document.querySelectorAll(".mynames");
    var variabs = {};
    // pentru pseudocod trebuie construit un obiect json care sa contina informatiile necesare
    for (let varname of names)  // pentru fiecare nume de variabila
    {
        // se obtine tipul si dimensionalitatea
        var id = varname.id.split('_')[1];
        var vtype = document.getElementById("vartype_"+id).value;
        variabs[varname.value] = {};
        variabs[varname.value]["type"] = vtype;
        var kind = document.getElementById("varkind_"+id).value;
        if (kind == "var")  // dimensionalitate 0
        {
            variabs[varname.value]["ndims"] = 0;
        }
        else if (kind == "arr") // array, dimensionalitate 1
        {
            var d1 = parseInt(document.getElementById("dim1_"+id).value);
            variabs[varname.value]["ndims"] = 1;
            variabs[varname.value]["dims"] = [d1];
        }
        else // matrice, dimensionalitate 2
        {
            var d1 = parseInt(document.getElementById("dim1_"+id).value);
            var d2 = parseInt(document.getElementById("dim2_"+id).value);
            variabs[varname.value]["ndims"] = 2;
            variabs[varname.value]["dims"] = [d1, d2];
        }
    }
    document.getElementById("overlay").style.display = "block";
    var activecar = $( ".carousel-item.active" )[0];    // problema activa
    var qid = activecar.getAttribute("data-prid");      // id-ul problemei
    // se apeleaza serviciul
    $.post('/compute/'+flang+'/'+tolang, { data: input, vars: JSON.stringify(variabs), quesid: qid}, function(result) {
        // se afiseaza ulterior rezultatul, in functie de data
        document.getElementById("overlay").style.display = "None";
        var now = new Date();
        var tolog = format_two(now.getHours()) + ":" + format_two(now.getMinutes()) + ":" + format_two(now.getSeconds());
        if (result.startsWith("nok:"))  // in caz de eroare, se afiseaza aecstea
        {
            result = result.slice(4);
            document.getElementById("arealog").value += tolog + " >>> " + result;
            document.getElementById("arearezultat").value = "";
        }
        else    // in caz de succes, se obtin datele utilizatorului ce se actualizeaza in interfata, apoi se afiseaza rezultatul
        {
            console.log(result);
            // datele actualizate ale utilizatorului se afla dupa un tag de tip <updateduserdata>
            if (!result.includes("<updateduserdata>"))
            {
                if (!result.includes("<invalidated>"))
                {
                    document.getElementById("arealog").value += tolog + " >>> Succes" + "\r\n";
                    document.getElementById("arearezultat").value = result;
                }
                else
                {
                    var splat = result.split("<invalidated>");
                    document.getElementById("arearezultat").value = splat[0];
                    document.getElementById("arealog").value += tolog + " >>> Programul nu conține erori, dar nu respectă funcționalitatea cerută!" + "\r\n";
                }
            }
            else
            {
                var splat = result.split("<updateduserdata>");
                document.getElementById("arearezultat").value = splat[0];
                document.getElementById("ascore").innerHTML = "Scor: "+splat[1].split(":")[0];
                document.getElementById("arank").innerHTML = "Rang: " + splat[1].split(":")[1];
                document.getElementById("arealog").value += tolog + " >>> Succes. " +splat[1].split(":")[2];
            }
        }
        document.getElementById("arealog").scrollTop = document.getElementById("arealog").scrollHeight;
    });
}

function adauga_variabila()     // functie pentru adaugarea unei variabile, la apasarea butonului din controlul de tip modal
{
    var vardiv = document.getElementById("variabile");
    // rand pentru variabila
    var selectorType = `<select name="vartype_`+adaugate+`" id="vartype_`+adaugate+`" class="custom-select mytypes">
        <option value="int" selected>Număr întreg</option>
        <option value="float">Număr rațional</option>
        <option value="char">Caracter</option>
    </select>
    `;
    // optiuni pentru dimensionalitate
    var selectorVar = `<select name="varkind_`+adaugate+`" id="varkind_`+adaugate+`" class="custom-select mykinds" onchange="afisDims(this)">
        <option value="var" selected>Variabilă</option>
        <option value="arr">Vector</option>
        <option value="mat">Matrice</option>
    </select>
    `;
    // optiuni pentru valoarea dimensionalitatii
    var dims = `
    <input type="text" class="form-control mydim1" id="dim1_`+adaugate+`" placeholder="Dimensiune 1" style="display: none;">
    <input type="text" class="form-control mydim2" id="dim2_`+adaugate+`" placeholder="Dimensiune 2" style="display: none;">
    `;
    // butonul de stergere
    var deletebuton = `<div class="input-group-append">
        <button class="btn btn-secondary" type="button" onclick="remove_variabila(this)">&#10006;</button>
    </div>`;
    // initializarea contextului ce trebuie adaugat in pagina
    var added = `<div class="input-group mb-3 grupuri" id="grup_`+adaugate+`">
        <input type="text" class="form-control mynames" id="varname_`+adaugate+`" placeholder="Nume">`;
    // sfarsitul contextului de adaugat
    var last = "</div>";
    // compunerea contextului
    added += selectorType;
    added += selectorVar;
    added += dims;
    added += deletebuton;
    added += last;
    // se adauga contextul
    vardiv.insertAdjacentHTML("beforeend", added);
    adaugate++;
}

function afisDims(caller)       // functie pentru afisarea dimensiunilor in controlul de tip modal la schimbarea valorii dimensionalitatii
{
    var num = caller.id.split('_')[1];
    var d1 = document.getElementById("dim1_"+num);
    var d2 = document.getElementById("dim2_"+num);
    if (caller.value == "arr")
    {
        d1.style.display = "inherit";
        d2.style.display = "none";
    }
    else if (caller.value == "mat")
    {
        d1.style.display = "inherit";
        d2.style.display = "inherit";
    }
    else
    {
        d1.style.display = "none";
        d2.style.display = "none";
    }
}

function remove_variabila(caller)       // functie pentru stergerea unei variabile din modalul de configurare
{
    var grup = caller.parentElement.parentElement;
    grup.remove();
}

function format_two(number)             // functie utilitara pentru formatarea datei
{
    if (number <= 9)
        return "0"+number
    else
        return number
}