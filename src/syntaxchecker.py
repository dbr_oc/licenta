from src.tokenizer import Tokenizer
from src.util import TokenTypePseudocode


class SyntaxChecker:
    @staticmethod
    def pseudocode_check(input_text):                               # metoda pentru verificarea sintaxei pseudocodului
        tok = Tokenizer(input_text)                                 # initializare tokenizer
        tok.tokenize_pseudocode()                                   # tokenizarea textului
        tokens = tok.token_list
        block_match = SyntaxChecker.__check_block_match(tokens)     # se verifica integritatea blocurilor existente
        begin_ok = SyntaxChecker.__check_begin(tokens)              # se verifica existenta blocului begin
        if begin_ok is not None:
            return begin_ok + "\r\n"
        elif block_match is not None:
            return block_match + "\r\n"
        else:                                                   
            return None

    @staticmethod
    def __check_begin(token_list):                                  # metoda pentru verificarea existentei blocului begin
        if token_list[0].type != TokenTypePseudocode.BEGIN:
            return "Programul trebuie să înceapă cu un bloc de tip BEGIN!"
        return None

    @staticmethod
    def __check_block_match(token_list):                            # metoda pentru verificarea integritatii blocurilor
        last_block = []                                             # coada de blocuri deschise
        for token in token_list:
            if token.type == TokenTypePseudocode.IF or \
               token.type == TokenTypePseudocode.BEGIN or \
               token.type == TokenTypePseudocode.FOR or \
               token.type == TokenTypePseudocode.REPEAT or \
               token.type == TokenTypePseudocode.WHILE:             # se salveaza ultimul bloc deschis in coada de blocuri deschise
                last_block.append(token.type)
            elif token.type == TokenTypePseudocode.UNTIL:           # la aparitia tokenului until se verifica daca un bloc repeat a fost deschis
                if len(last_block) > 0:
                    if last_block[-1] != TokenTypePseudocode.REPEAT:
                        if TokenTypePseudocode.REPEAT not in last_block:
                            return "Bloc lipsă. Nu există blocuri REPEAT de închis."
                        return "Blocuri încrucișate: REPEAT - " + last_block[-1]    # daca un alt bloc nu a fost inchis, exista blocuri incrucisate
                    else:
                        last_block.pop()
                else:
                    return "Bloc lipsă. Nu există blocuri REPEAT de închis."
            elif token.type == TokenTypePseudocode.END:             # la aparitia tokenului end se verifica daca blocurile care nu se termina cu end au fost inchise
                if len(last_block) > 0:                             # in acest caz, singurul block care nu se termina in end este repeat ... until
                    if last_block[-1] == TokenTypePseudocode.REPEAT:
                        if len(last_block) > 1:
                            return "Blocuri încrucișate: " + last_block[-2] + " - REPEAT"
                        else:
                            return "Bloc lipsă. Blocurile de tip REPEAT se închid cu UNTIL."
                    else:
                        if last_block[-1] == TokenTypePseudocode.IF or \
                           last_block[-1] == TokenTypePseudocode.BEGIN or \
                           last_block[-1] == TokenTypePseudocode.FOR or \
                           last_block[-1] == TokenTypePseudocode.REPEAT or \
                           last_block[-1] == TokenTypePseudocode.WHILE:
                            last_block.pop()                        # se elimina ultimul bloc introdus in coada
                        else:
                            return "Bloc necunoscut!"
                else:
                    return "Nu există blocuri de închis."
            elif token.type == TokenTypePseudocode.ELSE:            # la aparitia tokenului else se verifica daca apare
                if len(last_block) > 0:                             # in interiorul unui bloc if
                    if last_block[-1] != TokenTypePseudocode.IF:
                        if len(last_block) > 1:                     # atunci exista blocuri incrucisate
                            return "Blocuri încrucișate: ELSE - " + last_block[-1]
                        else:
                            if TokenTypePseudocode.IF not in last_block:    # sau nu exista blocuri if deschise
                                return "Nu există blocuri IF de închis."
                            else:
                                return "Instrucțiune ELSE amplasată greșit. Instrucțiunea ELSE trebuie plasată în interiorul unui bloc IF."
                else:
                    return "Nu există blocuri IF de închis."
            else:
                continue
        if len(last_block) > 0:     # in cazul in care coada nu este goala, au ramas blocuri neinchise
            return "Blocuri neînchise: " + ",".join(list(dict.fromkeys(last_block)))
        return None



