from src.util import TokenLinkValue
from src.util import UtilMethods


class Token(object):
    def __init__(self, partype, parvalue):      # constructor
        self.type = partype                     # tip token
        self.value = parvalue                   # valoare token

    def __str__(self):                          # metoda pentru transformarea unui token in string
        return 'Token({type}, {value})'.format(
            type=repr(self.type),
            value=repr(self.value)
        )


class Tokenizer(object):
    def __init__(self, input_text):
        self.token_list = list()            # lista finala de tokeni
        self.buffer = ""                    # bufferul in care se citeste fiecare token
        self.input_text = input_text        # text de tokenizat

    def check_and_add(self, check_dict):
        if self.buffer in list(check_dict.keys()):  # cuvant cheie
            self.token_list.append(Token(check_dict[self.buffer], self.buffer))
        else:
            self.add_name_or_number()

    def add_name_or_number(self):
        if UtilMethods.is_number(self.buffer):  # bufferul este un numar
            self.token_list.append(Token('NUMERIC', self.buffer))
        else:  # bufferul este un nume de functie sau de variabila
            self.token_list.append(Token('NAME', self.buffer))

    def check_double_char(self, caracter, neighbors, check_dict):
        # verifica si adauga tokeni formati din doua caractere care reprezinta tokeni independenti
        if self.buffer:  # exista date in buffer
            if self.buffer in list(check_dict.keys()):  # cuvant cheie
                if self.buffer in neighbors:
                    self.buffer += caracter
                    self.token_list.append(Token(check_dict[self.buffer], self.buffer))
                else:
                    self.token_list.append(Token(check_dict[self.buffer], self.buffer))
                self.buffer = ""  # se goleste bufferul
            else:
                self.add_name_or_number()
                self.buffer = caracter
        else:
            self.buffer += caracter

    def tokenize_c(self):
        string_started = False
        for caracter in self.input_text:    # se face o parcurgere caracter cu caracter
            if caracter.isspace():          # caracterul citit este spatiu deci tokenul trebuie salvat
                if self.buffer:             # daca bufferul nu este un sir gol atunci exista un token de salvat
                    self.check_and_add(TokenLinkValue.VALUE_TYPE_C)
                    self.buffer = ""        # se goleste bufferul
                    if caracter == "\n":
                        self.token_list.append(Token('NEWLINE', '\n'))
                    else:
                        if string_started:
                            self.token_list.append(Token('SPACE', caracter))
                else:                       # bufferul este gol
                    if caracter == "\n":
                        self.token_list.append(Token('NEWLINE', '\n'))
                    else:
                        if string_started:
                            self.token_list.append(Token('SPACE', caracter))
                    continue                # se trece la urmatoarea litera
            else:                           # caracterul nu este spatiu
                if caracter in list(TokenLinkValue.VALUE_TYPE_C.keys()):            # caracterul curent este un posibil token
                    if caracter == '.':     # caracterul . poate face parte dintr-un numar
                        if self.buffer.isdigit():                                   # bufferul este format din cifre
                            self.buffer += caracter                                 # caracterul va fi adaugat la buffer
                        else:
                            if self.buffer:  # exista date in buffer
                                self.check_and_add(TokenLinkValue.VALUE_TYPE_C)
                                self.buffer = ""  # se goleste bufferul
                            self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_C[caracter], caracter))
                    # se verifica si caracterele care pot face parte din tokeni compusi
                    # se verifica doar al doilea caracter, de exemplu <=, >=, == se verifica la =
                    elif caracter == "/":       # poate face parte din comentariu
                        self.check_double_char(caracter, ["/", "*"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "=":
                        self.check_double_char(caracter, [">", "<", "=", "!"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "&":
                        self.check_double_char(caracter, ["&"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "|":
                        self.check_double_char(caracter, ["|"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "*":
                        self.check_double_char(caracter, ["/"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "+":
                        self.check_double_char(caracter, ["+"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "-":
                        self.check_double_char(caracter, ["-"], TokenLinkValue.VALUE_TYPE_C)
                    elif caracter == "!" or caracter == "<" or caracter == ">":
                        if self.buffer:  # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                        self.buffer = caracter
                    elif caracter == '"':
                        # de la " se salveaza tot, inclusiv spatii
                        if string_started:
                            string_started = False
                        else:
                            string_started = True
                        if self.buffer:                                             # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_C)
                            self.buffer = ""  # se goleste bufferul
                        self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_C[caracter], caracter))  # caracterul
                    else:                       # restul caracterelor vor fi luate ca atare
                        if self.buffer:                                             # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_C)
                            self.buffer = ""  # se goleste bufferul
                        self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_C[caracter], caracter))  # caracterul
                else:   # caracter oarecare
                    if self.buffer in list(TokenLinkValue.VALUE_TYPE_C.keys()) and not self.buffer.isalnum():  # cuvant cheie
                        self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_C[self.buffer], self.buffer))
                        self.buffer = caracter
                    else:
                        self.buffer += caracter
        if self.buffer:
            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)

    def tokenize_pseudocode(self):
        string_started = False
        for caracter in self.input_text:    # se face o parcurgere caracter cu caracter
            if caracter.isspace():          # caracterul citit este spatiu deci tokenul trebuie salvat
                if self.buffer:             # daca bufferul nu este un sir gol atunci exista un token de salvat
                    self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                    self.buffer = ""        # se goleste bufferul
                    if caracter == "\n":
                        self.token_list.append(Token('NEWLINE', '\n'))
                    else:
                        if string_started:
                            self.token_list.append(Token('SPACE', caracter))
                else:                       # bufferul este gol
                    if caracter == "\n":
                        self.token_list.append(Token('NEWLINE', '\n'))
                    else:
                        if string_started:
                            self.token_list.append(Token('SPACE', caracter))
                    continue                # se trece la urmatoarea litera
            else:                           # caracterul nu este spatiu
                if caracter in list(TokenLinkValue.VALUE_TYPE_PSEUDOCODE.keys()):            # caracterul curent este un posibil token
                    if caracter == '.':     # caracterul . poate face parte dintr-un numar
                        if self.buffer.isdigit():                                   # bufferul este format din cifre
                            self.buffer += caracter                                 # caracterul va fi adaugat la buffer
                        else:
                            if self.buffer:  # exista date in buffer
                                self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                                self.buffer = ""  # se goleste bufferul
                            self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_PSEUDOCODE[caracter], caracter))
                    # se verifica si caracterele care pot face parte din tokeni compusi
                    # se verifica doar al doilea caracter, de exemplu <=, >=, == se verifica la =
                    elif caracter == "/":       # poate face parte din comentariu
                        self.check_double_char(caracter, ["/", "*"], TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                    elif caracter == "=":
                        self.check_double_char(caracter, [">", "<", "!"], TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                    elif caracter == "*":
                        self.check_double_char(caracter, ["/"], TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                    elif caracter == "+":
                        self.check_double_char(caracter, ["+"], TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                    elif caracter == "-":
                        self.check_double_char(caracter, ["-", "<"], TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                    elif caracter == "<" or caracter == ">":
                        if self.buffer:  # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                        self.buffer = caracter
                    elif caracter == "!":
                        if self.buffer:  # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                        self.buffer = caracter
                    elif caracter == '"':
                        # de la " se salveaza tot, inclusiv spatii
                        if string_started:
                            string_started = False
                        else:
                            string_started = True
                        if self.buffer:                                             # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                            self.buffer = ""  # se goleste bufferul
                        self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_PSEUDOCODE[caracter], caracter))  # caracterul
                    else:                       # restul caracterelor vor fi luate ca atare
                        if self.buffer:                                             # exista date in buffer
                            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)
                            self.buffer = ""  # se goleste bufferul
                        self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_PSEUDOCODE[caracter], caracter))  # caracterul
                else:   # caracter oarecare
                    if self.buffer in list(TokenLinkValue.VALUE_TYPE_PSEUDOCODE.keys()) and not self.buffer.isalnum():  # cuvant cheie
                        self.token_list.append(Token(TokenLinkValue.VALUE_TYPE_PSEUDOCODE[self.buffer], self.buffer))
                        self.buffer = caracter
                    else:
                        self.buffer += caracter
        if self.buffer:
            self.check_and_add(TokenLinkValue.VALUE_TYPE_PSEUDOCODE)


if __name__ == "__main__":
    a = """
                #include <stdio.h>
                int main() {
                    int number1, number2, sum;
                    int a[] = {0,1,2,3}, b[10];
                    int c[2];
                    int d[][3] = {{1,2,3}, {4,5,6}};
                    printf("Enter two integers: ");
                    scanf("%d %d", &number1, &number2);
                    // calculating sum
                    /* unu
                    doi
                    trei */
                    sum = number1 + number2;
                    if ((2 > 1) && (1 < 2))
                    {
                        printf(true);
                    }
                    while (1 > 2)
                    {
                        a[1] = 0
                    }
                    printf("%d + %d = %d", number1, number2, sum);
                    int i;
                    for (i = 0; i < 2; i++)
                    {
                        // nimic
                    }
                    return 0;
                }"""
    # a = """
    #     FOR X = 1 to 10
    #             FOR Y = 1 to 10
    #                 IF gameBoard[X][Y] = 0 THEN
    #                     // Do nothing
    #                 ELSE
    #                     theCall(X, Y)
    #                     counter++
    #                 END
    #             END
    #         END
    #         var a, b
    #         WHILE X!= Y THEN
    #             a <- a + 2
    #             b <- b - a
    #         END
    # """
    t = Tokenizer(a)
    t.tokenize_c()
    # for tok in t.token_list:
    #     print(str(tok))
    # t.tokenize_pseudocode()
    for tok in t.token_list:
        print(str(tok))





