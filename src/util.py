

class TokenType:
    # cuvinte cheie in C
    #
    INTEGER = 'INTEGER'
    DOUBLE = 'DOUBLE'
    FLOAT = 'FLOAT'
    BOOL = 'BOOL'
    CHAR = 'CHAR'
    #
    IF = 'IF'
    ELSE = 'ELSE'
    FOR = 'FOR'
    WHILE = 'WHILE'
    #
    LCBRACE = 'LCBRACE'             # {   left curly brace
    RCBRACE = 'RCBRACE'             # }   right curly brace
    LBRACE = 'LBRACE'               # [   left brace
    RBRACE = 'RBRACE'               # ]   right brace
    LPAREN = 'LPAREN'               # (
    RPAREN = 'RPAREN'               # )
    COMMENT = 'COMMENT'             # //
    OPENCOMMENT = 'OPENCOMMENT'     # /*
    CLOSECOMMENT = 'CLOSECOMMENT'   # */
    DQMARK = 'DQMARK'               # "
    SQMARK = 'SQMARK'               # '
    #
    PLUS = 'PLUS'
    INCREMENT = 'INCREMENT'
    DECREMENT = 'DECREMENT'
    MINUS = 'MINUS'
    MUL = 'MUL'
    DIV = 'DIV'
    MOD = 'MOD'
    EQUAL = 'EQUAL'
    NEQUAL = 'NEQUAL'
    NOT = 'NOT'
    LT = 'LT'                       # <
    GT = 'GT'                       # >
    LTE = 'LTE'                     # <=
    GTE = 'GTE'                     # >=
    ATRIB = 'ATRIB'
    AND = 'AND'
    OR = 'OR'
    XOR = 'XOR'
    AMPERSAND = 'AMPERSAND'
    BITOR = 'BITOR'
    DEFINE = 'DEFINE'
    INCLUDE = 'INCLUDE'
    RETURN = 'RETURN'
    COMMA = 'COMMA'             # ,
    DOT = 'DOT'                 # .
    SEMICOLON = 'SEMICOLON'     # ;
    NEWLINE = 'NEWLINE'         # \n
    TRUE = 'TRUE'
    FALSE = 'FALSE'


class TokenTypePseudocode:
    VAR = 'VAR'
    BEGIN = 'BEGIN'
    IF = 'IF'
    ELSE = 'ELSE'
    FOR = 'FOR'
    WHILE = 'WHILE'
    THEN = 'THEN'
    END = 'END'
    REPEAT = 'REPEAT'
    UNTIL = 'UNTIL'
    CASE = 'CASE'
    IN = 'IN'
    TO = 'TO'
    LBRACE = 'LBRACE'
    RBRACE = 'RBRACE'
    LPAREN = 'LPAREN'
    RPAREN = 'RPAREN'
    COMMENT = 'COMMENT'
    OPENCOMMENT = 'OPENCOMMENT'
    CLOSECOMMENT = 'CLOSECOMMENT'
    DQMARK = 'DQMARK'
    SQMARK = 'SQMARK'
    PLUS = 'PLUS'
    INCREMENT = 'INCREMENT'
    DECREMENT = 'DECREMENT'
    MINUS = 'MINUS'
    MUL = 'MUL'
    DIV = 'DIV'
    MOD = 'MOD'
    EQUAL = 'EQUAL'
    NEQUAL = 'NEQUAL'
    NOT = 'NOT'
    LT = 'LT'
    GT = 'GT'
    LTE = 'LTE'
    GTE = 'GTE'
    ATRIB = 'ATRIB'
    AND = 'AND'
    OR = 'OR'
    XOR = 'XOR'
    AMPERSAND = 'AMPERSAND'
    BITOR = 'BITOR'
    RETURN = 'RETURN'
    COMMA = 'COMMA'
    DOT = 'DOT'
    NEWLINE = 'NEWLINE'
    TRUE = 'TRUE'
    FALSE = 'FALSE'
    PRINT = 'PRINT'
    READ = 'READ'


class TokenLinkValue:
    # dictionar de forma token : corespondent_C
    TYPE_VALUE_C = {
        'INTEGER': 'int',
        'DOUBLE': 'double',
        'FLOAT': 'float',
        'BOOL': 'bool',
        'CHAR': 'char',
        'IF': 'if',
        'ELSE': 'else',
        'FOR': 'for',
        'WHILE': 'while',
        'LCBRACE': '{',
        'RCBRACE': '}',
        'LBRACE': '[',
        'RBRACE': ']',
        'LPAREN': '(',
        'RPAREN': ')',
        'COMMENT': '//',
        'OPENCOMMENT': '/*',
        'CLOSECOMMENT': '*/',
        'DQMARK': '\"',
        'SQMARK': '\'',
        'PLUS': '+',
        'INCREMENT': '++',
        'DECREMENT': '--',
        'MINUS': '-',
        'MUL': '*',
        'DIV': '/',
        'MOD': '%',
        'EQUAL': '==',
        'NEQUAL': '!=',
        'NOT': '!',
        'LT': '<',
        'GT': '>',
        'LTE': '<=',
        'GTE': '>=',
        'ATRIB': '=',
        'AND': '&&',
        'OR': '||',
        'XOR': '^',
        'AMPERSAND': '&',
        'BITOR': '|',
        'DEFINE': '#define',
        'INCLUDE': '#include',
        'RETURN': 'return',
        'COMMA': ',',
        'DOT': '.',
        'SEMICOLON': ';',
        'TRUE': 'true',
        'FALSE': 'false',
        'NEWLINE': '\n'
    }
    VALUE_TYPE_C = {
        'int': 'INTEGER',
        'double': 'DOUBLE',
        'float': 'FLOAT',
        'bool': 'BOOL',
        'char': 'CHAR',
        'if': 'IF',
        'else': 'ELSE',
        'for': 'FOR',
        'while': 'WHILE',
        '{': 'LCBRACE',
        '}': 'RCBRACE',
        '[': 'LBRACE',
        ']': 'RBRACE',
        '(': 'LPAREN',
        ')': 'RPAREN',
        '//': 'COMMENT',
        '/*': 'OPENCOMMENT',
        '*/': 'CLOSECOMMENT',
        '\"': 'DQMARK',
        '\'': 'SQMARK',
        '+': 'PLUS',
        '++': 'INCREMENT',
        '-': 'MINUS',
        '--': 'DECREMENT',
        '*': 'MUL',
        '/': 'DIV',
        '%': 'MOD',
        '==': 'EQUAL',
        '!=': 'NEQUAL',
        '!': 'NOT',
        '<': 'LT',
        '>': 'GT',
        '<=': 'LTE',
        '>=': 'GTE',
        '=': 'ATRIB',
        '&&': 'AND',
        '||': 'OR',
        '^': 'XOR',
        '&': 'AMPERSAND',
        '|': 'BITOR',
        '#define': 'DEFINE',
        '#include': 'INCLUDE',
        'return': 'RETURN',
        ',': 'COMMA',
        '.': 'DOT',
        ';': 'SEMICOLON',
        'true': 'TRUE',
        'false': 'FALSE',
        '\n': 'NEWLINE'
    }

    TYPE_VALUE_PSEUDOCODE = {
        'VAR': "var",
        'BEGIN': 'BEGIN',
        'IF': "IF",
        'ELSE': "ELSE",
        'FOR': "FOR",
        'WHILE': "WHILE",
        'THEN': "THEN",
        'END': "END",
        'REPEAT': "REPEAT",
        'UNTIL': "UNTIL",
        'CASE': "CASE",
        'IN': "IN",
        'TO': "TO",
        'LBRACE': "[",
        'RBRACE': "]",
        'LPAREN': "(",
        'RPAREN': ")",
        'COMMENT': "//",
        'OPENCOMMENT': "/*",
        'CLOSECOMMENT': "*/",
        'DQMARK': '"',
        'SQMARK': "'",
        'PLUS': "+",
        'INCREMENT': "++",
        'DECREMENT': "--",
        'MINUS': "-",
        'MUL': "*",
        'DIV': "/",
        'MOD': "%",
        'EQUAL': "=",
        'NEQUAL': "!=",
        'NOT': '!',
        'LT': "<",
        'GT': ">",
        'LTE': "<=",
        'GTE': ">=",
        'ATRIB': "<-",
        'AND': "AND",
        'OR': "OR",
        'XOR': "^",
        'AMPERSAND': "&",
        'BITOR': "|",
        'RETURN': "RETURN",
        'COMMA': ",",
        'DOT': ".",
        'NEWLINE': "\n",
        'TRUE': "TRUE",
        'FALSE': "FALSE",
        'PRINT': "PRINT",
        'READ': "READ"
    }

    VALUE_TYPE_PSEUDOCODE = {
        'var': "VAR",
        'BEGIN': 'BEGIN',
        'IF': "IF",
        'ELSE': "ELSE",
        'FOR': "FOR",
        'WHILE': "WHILE",
        'THEN': "THEN",
        'END': "END",
        'REPEAT': "REPEAT",
        'UNTIL': "UNTIL",
        'CASE': "CASE",
        'IN': "IN",
        'TO': "TO",
        '[': "LBRACE",
        ']': "RBRACE",
        '(': "LPAREN",
        ')': "RPAREN",
        '//': "COMMENT",
        '/*': "OPENCOMMENT",
        '*/': "CLOSECOMMENT",
        '"': 'DQMARK',
        "'": "SQMARK",
        '+': "PLUS",
        '++': "INCREMENT",
        '--': "DECREMENT",
        '-': "MINUS",
        '*': "MUL",
        '/': "DIV",
        '%': "MOD",
        '=': "EQUAL",
        '!=': "NEQUAL",
        '!': 'NOT',
        '<': "LT",
        '>': "GT",
        '<=': "LTE",
        '>=': "GTE",
        '<-': "ATRIB",
        'AND': "AND",
        'OR': "OR",
        '^': "XOR",
        '&': "AMPERSAND",
        '|': "BITOR",
        'RETURN': "RETURN",
        ',': "COMMA",
        '.': "DOT",
        '\n': "NEWLINE",
        'TRUE': "TRUE",
        'FALSE': "FALSE",
        'PRINT': "PRINT",
        'READ': "READ"
    }


class Operators:
    NUMBER_OPERATORS = [
        "+",
        "-",
        "*",
        "/",
        "%",
        "==",
        "!=",
        "<",
        ">",
        "<=",
        ">=",
        "AND",
        "OR"
    ]
    VARIABLE_OPERATORS = [
        "+",
        "-",
        "*",
        "/",
        "%",
        "==",
        "!=",
        "<",
        ">",
        "<=",
        ">=",
        "AND",
        "OR",
        "++",
        "--"
    ]


class Instructions:
    BEFORE_INSTRUCTIONS = [
        "WHILE", "FOR", "IF", "VAR"
    ]
    AFTER_INSTRUCTIONS = [
        "UNTIL", "ELSE", "END", "THEN", "*/", "REPEAT"
    ]


class PackageEquivalent:
    Packages = {
        "stdio.h": ""
    }


class FunctionUtil:
    PRINTF_FORMATERS = {"d": "int({0})",
                        "i": "int({0})",
                        "u": "ctypes.c_ulong({0}).value",
                        "o": "int({0}, 8)",
                        "x": "int({0}, 16)",
                        "X": "int({0}, 16)",
                        "f": "float({0})",
                        "F": "float({0})",
                        "e": "float({0})",
                        "E": "float({0})",
                        "g": "float({0})",
                        "G": "float({0})",
                        "a": "float({0})",
                        "A": "float({0})",
                        "c": "\"{0}\"",
                        "s": "\"{0}\"",
                        "p": "",
                        "n": "",
                        "%": ""
                        }
    PSEUDO_READ_FORMATERS = {
        "int": "%d",
        "float": "%f",
        "double": "%lf"
    }


class UtilMethods:
    @staticmethod
    def is_number(text):            # verifica daca un text poate reprezenta un numar
        if text.isdigit():
            return True
        if text.replace('.', '', 1).isdigit():
            return True
        text = text.replace('.', '', 1)
        if text[1:].isdigit():
            return True
        return False

    @staticmethod
    def is_integer(text):           # verifica daca un text este numar intreg
        if text.isdigit():
            return True
        if text[1:].isdigit():
            return True
        return False
