from flask import Flask, render_template, redirect, request, session, url_for
from flask_cors import CORS
from src.dbcom import DBCommunication
from src.translator import Translator
from threading import Lock
from json import loads
from distutils.ccompiler import new_compiler
from distutils.errors import CompileError
from src.syntaxchecker import SyntaxChecker
from passlib.context import CryptContext
import os
import sys
import time
import src.constants as constants
import secrets
import pymongo


dbclient = pymongo.MongoClient("mongodb://localhost:27017/")        # conexiunea cu serverul de baze de date, locala, portul 27017


app = Flask(__name__)                                               # aplicatia flask
app.config['SECRET_KEY'] = secrets.token_urlsafe(16)                # configurarea unei chei secrete aleatoare
CORS(app)                                                           # configurarea politicii cross origin
app.config.from_object(__name__)
session_index = 0                                                   # index folosit pentru controlul accesului
session_lock = Lock()                                               # obiect de tip lock pentru controlul obiectelor de sesiune
db_lock = Lock()                                                    # obiect de tip lock pentru accesul la baza de date


@app.route('/')                                                     # ruta pentru pagina principala
def home():
    if "username" in session:                                       # ia variabila username daca exista in sesiune
        username = session["username"]                              # cat si datele utilizatorului
        userdata = DBCommunication.get_user_rank_score(dbclient, username)
    else:
        username = None
        userdata = None
    try:                                                            # in caz de eroare, pagina contine un mesaj specific
        error = request.args["error"]
        return render_template('index.html', error=error, utilizator=username, userdata=userdata)
    except:
        return render_template('index.html', utilizator=username, userdata=userdata)


@app.route('/beginner')                                             # pagina specifica nivelului incepator
def beginner():
    if "username" in session:                                       # vezi linia 34
        username = session["username"]
        userdata = DBCommunication.get_user_rank_score(dbclient, username)
    else:
        username = None
        userdata = None
    prs, ids = DBCommunication.questions_and_ids(dbclient, "easy")  # extrage intrebari din baza de date
    return render_template('beginner.html', probleme=prs, ids=ids, utilizator=username, userdata=userdata)


@app.route('/intermediate')                                         # pagina specifica nivelului intermediar
def intermediate():                                                 # similara beginner
    if "username" in session:
        username = session["username"]
        userdata = DBCommunication.get_user_rank_score(dbclient, username)
    else:
        username = None
        userdata = None
    prs, ids = DBCommunication.questions_and_ids(dbclient, "average")
    return render_template('translate.html',
                           surse=constants.LIMBAJE_SURSA_INTERMEDIATE[0],
                           rezultate=list(constants.LIMBAJE_REZULTAT[constants.LIMBAJE_SURSA_INTERMEDIATE[0]].values())[0],
                           probleme=prs,
                           ids=ids,
                           titlu="Nivel intermediar",
                           utilizator=username,
                           userdata=userdata)


@app.route('/advanced')                                             # pagina specifica utilizatori
def advanced():                                                     # idem intermediate
    if "username" in session:
        username = session["username"]
        userdata = DBCommunication.get_user_rank_score(dbclient, username)
    else:
        username = None
        userdata = None
    prs, ids = DBCommunication.questions_and_ids(dbclient, "hard")
    return render_template('translate.html',
                           surse=constants.LIMBAJE_SURSA_ADVANCED[0],
                           rezultate=list(constants.LIMBAJE_REZULTAT[constants.LIMBAJE_SURSA_ADVANCED[0]].values())[0],
                           probleme=prs,
                           ids=ids,
                           titlu="Nivel avansat",
                           utilizator=username,
                           userdata=userdata)


@app.route('/toolbox')                                              # returneaza resursa specifica toolboxului blockly
def toolbox():
    with open('static\\toolbox.xml', 'r', encoding='utf-8') as f:
        data = f.read()
    return data


@app.route('/login')                                                # pagina de autentificare si inregistrare
def log_page():
    if "username" in session:                                       # daca exista un utilizator logat, se returneaza eroare
        return redirect(url_for('.home', error="Trebuie să vă delogați mai întâi!"))
    return render_template('login.html', titlu="Log in")


@app.route('/log', methods=['POST'])                                # serviciu de autentificare
def log():
    if "username" in session:                                       # un utilizator logat nu poate accesa serviciul
        return "Acțiune interzisă!"
    # se descrie un context de criptare a parolei introduse
    password_context = CryptContext(schemes=["pbkdf2_sha256"], default="pbkdf2_sha256", pbkdf2_sha256__default_rounds=30000)
    username = request.form["user"]
    password = request.form["pass"]
    try:
        # se verifica potrivirea parolei introduse cu cea din baza de date
        if password_context.verify(password, DBCommunication.get_pass(dbclient, username)):
            session["username"] = username
            return "ok"
        else:
            return "Nume de utilizator sau parolă incorectă!"
    except:     # in caz de eroare
        return "Nume de utilizator sau parolă incorectă!"


@app.route('/signin', methods=['POST'])                             # serviciu de inregistrare
def signin():
    if "username" in session:                                       # interzis utilizatorilor autentificati
        return "Acțiune interzisă!"
    # descriere context criptare
    password_context = CryptContext(schemes=["pbkdf2_sha256"], default="pbkdf2_sha256",
                                    pbkdf2_sha256__default_rounds=30000)
    username = request.form["user"]
    password = request.form["pass"]
    try:
        try:
            # sectiune critica
            db_lock.acquire()
            # introducere utilizator nou in baza de date
            DBCommunication.new_user(dbclient, username, password_context.hash(password))
            session["username"] = username
            return "ok"
        except:
            return "Numele de utilizator este deja folosit!"
        finally:
            # parasire sectiune critica
            db_lock.release()
    except:
        return "Numele de utilizator este deja folosit!"


@app.route('/logout')               # serviciu de delogare
def logout():                       # se sterge din sesiune variabila username
    session.pop("username", None)
    return redirect('/')


@app.route('/rezultate/<sursa>')    # serviciu pentru returnarea corespondentilor unui limbaj
def get_corespondenti_sursa(sursa):
    return str(list(constants.LIMBAJE_REZULTAT[sursa].values()))[1:-1].replace("'", "")


@app.route('/compute/<flang>/<tolang>', methods=['POST'])   # serviciu de traducere
def translate_input(flang, tolang):
    global session_index, session_lock
    session_lock.acquire()                                  # sectiune critica
    try:
        received = request.form["data"]                     # informatia de tradus
        received = received.replace('\xa0', ' ')
        c_invalidated = False
        scor = 0
        if "index" in session:  # si se verifica erorile aparute
            my_index = session["index"]  # se stabileste un index pentru un utilizator
        else:
            my_index = session_index
            session["index"] = my_index
            session_index += 1
        if flang == "C":                                    # caz special pentru limbajul C, se compileaza codul
            out_name = "../out_"+str(my_index)+".txt"       # stabilirea fisierelor de iesire pentru compilare
            err_name = "../err_"+str(my_index)+".txt"
            in_name = "../in_"+str(my_index)+".c"
            ob_name = "../in_"+str(my_index)+".obj"

            exe_in = "exein" + str(my_index) + ".txt"
            exe_out = "exeout" + str(my_index) + ".txt"
            mainexe = "main" + str(my_index)

            quesid = request.form["quesid"]

            with open(in_name, "w") as inn:
                inn.write(received)
            # compiler nou
            comp = new_compiler()
            # salvare output normal si de erori standard
            oldstderr = os.dup(sys.stderr.fileno())
            oldstdout = os.dup(sys.stdout.fileno())
            err = open(err_name, 'w')
            out = open(out_name, 'w')
            # modificare stdout si stderr
            os.dup2(err.fileno(), sys.stderr.fileno())
            os.dup2(out.fileno(), sys.stdout.fileno())
            error_message = None
            probout = ""
            try:
                # compilare
                comp.compile([in_name], extra_postargs=["-w"])
                comp.link_executable([ob_name], mainexe)
            except CompileError as er:
                # eroare la compilare
                error_message = str(er)
            finally:
                # refacerea outputului
                os.dup2(oldstderr, sys.stderr.fileno())
                os.dup2(oldstdout, sys.stdout.fileno())
                out.close()
                err.close()
            # asteapta compilarea (proces independent)
            # fisierele out_name si err_name apar doar dupa inchiderea lor
            while not os.path.exists(out_name) or not os.path.exists(err_name):
                time.sleep(0.5)
            # obtinerea erorilor
            code_errors = parse_c_errors(out_name, in_name)
            # lista de fisiere generate in urma compilarii
            generated = [out_name, err_name, in_name, ob_name]
            # stergerea fisierelor generate
            discard_output(generated)
            # in cazul unor erori, se returneaza acestea
            if error_message is not None:
                return "nok:"+code_errors
            else:

                inouts = DBCommunication.get_in_out(dbclient, "hard", quesid)
                for doc in inouts:
                    probin = doc["in"]
                    probout = doc["out"]
                    with open(exe_in, "w") as fin:
                        fin.write(probin)
                    command = mainexe+" <" + exe_in + " >" + exe_out
                    os.system(command)
                    max_count = 10
                    time_passed = 0
                    while not os.path.exists(exe_out):
                        time.sleep(0.5)
                        time_passed += 1
                        if time_passed >= max_count:
                            os.system("TASKKILL /F /IM "+mainexe)
                            discard_output([exe_in, exe_out, mainexe])
                            c_invalidated = True
                            break
                    with open(exe_out, "r") as eout:
                        givenout = eout.read()
                    if output_valid(probout, givenout):
                        scor += 10
                    discard_output([exe_out])
                if scor == 0:
                    c_invalidated = True
                discard_output([exe_in, exe_out, mainexe])
        elif flang == "PSEUDOCOD":                                  # caz special, pseudocod
            ps_error = SyntaxChecker.pseudocode_check(received)     # se verifica sintaxa
            if ps_error is not None:                                # in caz de eroare se returneaza acestea
                return "nok:" + ps_error
        variables = loads(request.form["vars"])                     # se retin variabilele definite
        if flang != "PSEUDOCOD":                                    # doar pentru pseudocod
            variables = None
        t = Translator(received, flang, tolang, variables)          # initializare translator
        t.translate()                                               # traducere
        t.eliminate_spaces()                                        # curatare output
        if flang == "PSEUDOCOD":
            out_name = "../out_" + str(my_index) + ".txt"           # stabilirea fisierelor de iesire pentru compilare
            err_name = "../err_" + str(my_index) + ".txt"
            in_name = "../in_" + str(my_index) + ".c"
            ob_name = "../in_" + str(my_index) + ".obj"

            exe_in = "exein"+str(my_index) + ".txt"
            exe_out = "exeout"+str(my_index) + ".txt"
            mainexe = "main" + str(my_index)

            quesid = request.form["quesid"]

            with open(in_name, "w") as inn:
                inn.write(t.output)
            # compiler nou
            comp = new_compiler()
            # salvare output normal si de erori standard
            oldstderr = os.dup(sys.stderr.fileno())
            oldstdout = os.dup(sys.stdout.fileno())
            err = open(err_name, 'w')
            out = open(out_name, 'w')
            # modificare stdout si stderr
            os.dup2(err.fileno(), sys.stderr.fileno())
            os.dup2(out.fileno(), sys.stdout.fileno())
            error_message = None
            probout = ""
            try:
                # compilare
                comp.compile([in_name], extra_postargs=["-w"])
                comp.link_executable([ob_name], mainexe)
            except CompileError as er:
                # eroare la compilare
                error_message = str(er)
            finally:
                # refacerea outputului
                os.dup2(oldstderr, sys.stderr.fileno())
                os.dup2(oldstdout, sys.stdout.fileno())
                out.close()
                err.close()
            # asteapta compilarea (proces independent)
            # fisierele out_name si err_name apar doar dupa inchiderea lor
            while not os.path.exists(out_name) or not os.path.exists(err_name):
                time.sleep(0.5)
            generated = [out_name, err_name, in_name, ob_name]
            # stergerea fisierelor generate
            discard_output(generated)
            # in cazul unor erori, se returneaza acestea
            if error_message is not None:
                print(error_message)
                return "nok:" + "Programul scris conține erori!\r\n"
            else:   # executare pentru verificare
                # probin, probout = DBCommunication.get_in_out(dbclient, "average", quesid)
                inouts = DBCommunication.get_in_out(dbclient, "average", quesid)
                for doc in inouts:
                    probin = doc["in"]
                    probout = doc["out"]
                    with open(exe_in, "w") as fin:
                        fin.write(probin)
                    command = mainexe+" <" + exe_in + " >" + exe_out
                    os.system(command)
                    max_count = 10
                    time_passed = 0
                    while not os.path.exists(exe_out):
                        time.sleep(0.5)
                        time_passed += 1
                        if time_passed >= max_count:
                            os.system("TASKKILL /F /IM "+mainexe)
                            discard_output([exe_in, exe_out, mainexe])
                            return t.output + "<invalidated>"
                    with open(exe_out, "r") as eout:
                        givenout = eout.read()
                    discard_output([exe_out])
                    if output_valid(probout, givenout):
                        scor += 5
                if scor == 0:
                    return t.output + "<invalidated>"
                discard_output([exe_in, exe_out, mainexe])
        if c_invalidated:
            return t.output + "<invalidated>"
        if "username" in session:                                   # calculul scorului
            quesid = request.form["quesid"]
            if not DBCommunication.ques_in_solved(dbclient, session["username"], quesid):   # verificare intrebarii
                DBCommunication.add_score(dbclient, session["username"], scor, quesid)      # update scor
                scor = "Scor acordat:" + str(scor) + "\r\n"
            else:
                scor = "Problema a fost deja rezolvată, nu se acordă punctaj suplimentar!\r\n"
            updated = DBCommunication.get_user_rank_score(dbclient, session["username"])    # date modificate
            return t.output + "<updateduserdata>" + str(updated[0]) + ":" + str(updated[1]) + ":" + scor
        return t.output
    except Exception as e:                                          # in caz de eroare la procesul de traducere
        import traceback
        traceback.print_tb(e.__traceback__)
        print(e)
        return "nok:Eroare de sintaxă. Vă rugăm verificați codul scris pentru greșeli.\r\n"
    finally:
        session_lock.release()


def parse_c_errors(filename, to_remove):                            # functie pentru extragerea erorilor din fisierul de
    with open(filename) as rez:                                     # iesire
        code_errors = rez.read()
    lines = code_errors.splitlines()                                # se elimina numele fisierului in care apare eroare
    output = ""                                                     # si se pastreaza doar linia si eroare
    # se sare prima linie care reprezinta numele fisierului compilat
    for i in range(1, len(lines)):
        remline = lines[i].replace(to_remove, "")
        output += remline + "\r\n"
    return output


def discard_output(file_list):                                      # stergerea unei liste de fisiere introduse
    for file in file_list:
        if os.path.exists(file):
            os.remove(file)


def output_valid(prefered, given):
    prefered = prefered.strip()
    if ',' in given:
        given = given.replace(',', ' ')
    given = ' '.join(given.split())
    return prefered == given


if __name__ == '__main__':                                          # main
    app.run(debug=True, port=5000)                                  # rularea aplicatiei
