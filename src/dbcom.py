import random
from bson.objectid import ObjectId


class DBCommunication:
    @staticmethod
    def get_user_rank_score(dbclient, username):            # metoda pentru obtinerea datelor despre rank si scor
        database = dbclient["webappdb"]                     # a unui utilizator
        collection = database["utilizatori"]
        rezultat = collection.find_one({"username": username})
        scor = rezultat["score"]
        ranks = database["ranks"]
        rank = ranks.find_one({"minval": {'$lte': scor}, "maxval": {'$gte': scor}})["name"]
        rval = [scor, rank]
        return rval

    @staticmethod
    def add_score(dbclient, username, value, question):     # adaugarea unui punctaj
        database = dbclient["webappdb"]
        collection = database["utilizatori"]
        collection.update_one({"username": username}, {"$inc": {"score": value}, "$push": {"solved": question}})

    @staticmethod
    def ques_in_solved(dbclient, username, question_id):    # metoda ce verifica daca o problema a fost rezolvata
        database = dbclient["webappdb"]
        collection = database["utilizatori"]
        rezultat = collection.find_one({"username": username})
        if question_id in rezultat["solved"]:
            return True
        return False

    @staticmethod
    def questions_and_ids(dbclient, collection_name):       # metoda returneaza doua liste
        database = dbclient["webappdb"]                     # una dintre acestea este o lista cu probleme
        collection = database[collection_name]              # iar cealalta este o lista cu id-urile acestora
        quest = list()
        for pr in collection.find():
            quest.append(pr)
        samples = random.sample(quest, 5)
        prs = [p["text"] for p in samples]
        ids = [p["_id"] for p in samples]
        return prs, ids

    @staticmethod
    def get_in_out(dbclient, collection_name, prid):
        database = dbclient["webappdb"]
        collection = database[collection_name]
        rezultat = collection.find_one({"_id": ObjectId(prid)})
        return rezultat["datasets"]


    @staticmethod
    def get_pass(dbclient, username):                       # returneaza parola criptata a unui utilizator
        database = dbclient["webappdb"]
        collection = database["utilizatori"]
        rezultat = collection.find_one({"username": username})
        return rezultat["password"]

    @staticmethod
    def new_user(dbclient, username, password):             # introdcucerea unui nou utilizator in baza de date
        database = dbclient["webappdb"]
        collection = database["utilizatori"]
        rezultat = collection.find_one({"username": username})      # se incearca gasirea unui utilizator cu acelasi nume
        if rezultat:                                                # in acest caz se genereaza o exceptie
            raise Exception("Există deja")
        collection.insert_one({                                     # altfel se continua cu inserarea
            "username": username,
            "password": password,
            "score": 0,
            "solved": []
        })
