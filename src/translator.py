from src.tokenizer import Token, Tokenizer
from src.util import FunctionUtil, TokenType, PackageEquivalent, TokenTypePseudocode, Operators, Instructions


class Translator(object):
    def __init__(self, text, fromlang, tolang, variables=None):
        self.text = text                    # text pentru tokenizare
        self.fromlang = fromlang            # limbaj de programare in care este scris textul
        self.tolang = tolang                # limbaj de programare in care va fi translat textul
        self.tokenizer = Tokenizer(text)    # tokenizator folosit
        self.output = ""                    # output-ul ultimei translatari
        self.variables = variables          # in cazul pseudocodului este nevoie de variabile mapate cu tipul lor
        self.pseudo_read_index = 0          # index folosit pentru contorizarea citirilor astfel incat sa se goleasca bufferul

    @staticmethod
    def parse_c_function(input, indent):
        # inputul reprezinta apelul unei functii, deci functie(parametri)
        functie = input.split('(')[0]           # numele functiei
        parametri = input.split('(')[1][:-1]    # parametrii fara ultimul caracter care este )
        parametri = parametri.split(',')        # parametrii sunt despartiti de virgula
        # parsarea functiilor
        # principalele functii urmarite pentru parsare sunt printf si scanf
        out = ""
        if functie == "printf":
            # primul parametru este stringul de formatat
            # acesta se imparte dupa spatii si se cauta elementele care incep cu %
            # acestea se inlocuiesc cu {numar_parametru}
            parametri[0] = parametri[0].replace("\"", "", 1)    # se scoate primul "
            parametri[0] = parametri[0][::-1]                   # reverse
            parametri[0] = parametri[0].replace("\"", "", 1)    # se scoate ultimul "
            parametri[0] = parametri[0][::-1]                   # reverse
            string_formatare = parametri[0].split(' ')
            if len(parametri) > 1:
                index = 0
                found = 0
                for cuvant in string_formatare:
                    if cuvant.startswith("%"):
                        if len(cuvant) > 1 and cuvant[1] in list(FunctionUtil.PRINTF_FORMATERS.keys()):
                            # este cuvant formatat
                            string_formatare[index] = "{"+str(found)+"}"
                            found += 1
                    index += 1
                string_final = " ".join(string_formatare)
                out = " print(\"" + string_final + "\".format(" + ",".join(parametri[1:]) + "))"
            else:
                out = " print(\""+parametri[0]+"\")"
        elif functie == "scanf":
            parametri[0] = parametri[0].replace("\"", "", 1)  # se scoate primul "
            parametri[0] = parametri[0][::-1]  # reverse
            parametri[0] = parametri[0].replace("\"", "", 1)  # se scoate ultimul "
            parametri[0] = parametri[0][::-1]  # reverse
            string_formatare = parametri[0].split(' ')
            index = 0
            found = 0
            up_word = ""
            for cuvant in string_formatare:
                if cuvant.startswith("%"):
                    if len(cuvant) > 1 and cuvant[1] in list(FunctionUtil.PRINTF_FORMATERS.keys()):
                        # este cuvant formatat
                        string_formatare[index] = " " + parametri[1 + found].replace("&", "") + " = " +\
                                                  FunctionUtil.PRINTF_FORMATERS[cuvant[1]].format("input("+up_word+")")\
                                                  + "\n" + indent
                        found += 1
                        up_word = ""
                    else:
                        up_word += cuvant + " "
                        string_formatare[index] = ""
                else:
                    up_word += cuvant + " "
                    string_formatare[index] = ""
                index += 1
            out = "".join(string_formatare)
        else:
            # daca nu e o functie cunoscuta, ramane in aceeasi formula
            out = input
        return out

    @staticmethod
    def process_for(data):
        data = data.replace("for", "")
        data = data.replace("(", "", 1)
        data = data[:-1]
        data = data.split(";")
        start = data[0]
        conditie = data[1]
        continuare = data[2]
        start = start.split("=")        # variabila = valoare
        conditie = Translator.parse_conditie(conditie)
        continuare = Translator.parse_continuare(continuare)
        out = " for " + start[0] + " in range(" + start[1] + ", "+conditie+", " + continuare + "):"
        return out

    def pseudo_to_c(self):
        output = ""
        # tokenizarea textului
        self.tokenizer.tokenize_pseudocode()
        tokens = self.tokenizer.token_list
        # sectiune de initiere a flagurilor si variabilelor intermediare folosite
        dqmarks = False                     # flag pentru scrierea stringurilor
        indentation = ""                    # indentare pentru aspect
        paren_opened = False                # flag pentru scrierea conditiilor
        until_flag = False                  # flag pentru parsarea conditiilor
        for_flag = False                    # flag pentru parsarea forurilor
        for_data = ""                       # instructiunea for de parsat
        comment = False                     # flag pentru scrierea comentariilor
        multiple_comment = False            # flag pentru scrierea comentariilor pe mai multe linii
        read_flag = False                   # flag care spune daca instructiunea read a aparut
        read_data = ""                      # datele de parsat din instructiunea read
        print_flag = False                  # flag care spune daca instructiunea print a aparut
        print_data = ""                     # datele de parsat din instructiunea print
        last_line = ""                      # variabila ce descrie tipul ultimei instructiuni
        variables = self.variables
        for token in tokens:                # parcurgere tokeni
            if comment:                     # comentariile sunt prioritare
                if token.type == TokenTypePseudocode.NEWLINE:   # newline incheie un comentariu
                    output += token.value + indentation
                    comment = False
                    continue
                output += token.value + " "
                continue
            if multiple_comment:            # comentarii pe linii multiple prioritare
                if token.type == TokenTypePseudocode.CLOSECOMMENT:  # se reseteaza flagul la gasirea tokenului de inchidere
                    multiple_comment = False
                if token.type == TokenTypePseudocode.NEWLINE:       # la linie noua se continua comentariul
                    output += token.value + indentation
                else:
                    output += token.value + " "
                continue
            if read_flag:                   # in cazul aparitiei unei instructiuni read
                if token.type == TokenTypePseudocode.NEWLINE:   # se reseteaza la newline
                    read_flag = False
                    if output[-1] == "\t":
                        output = output[:-1*len(indentation)]
                    output += self.parse_read_pseudocode(read_data, indentation)    # se apeleaza functia de parsare a functiei read
                else:
                    read_data += token.value    # se adauga valoarea tokenului la datele de citit
                continue
            if print_flag:                  # similar cu read, apare in cazul aparitiei instructiunii print
                if token.type == TokenTypePseudocode.NEWLINE:   # flagul este resetat la linie noua
                    print_flag = False
                    if output[-1] == "\t":
                        output = output[:-1*len(indentation)]
                    output += self.parse_print_pseudocode(print_data, indentation)  # se apeleaza functia de parsare a instructiunii
                else:
                    print_data += token.value   # se adauga tokenul
                continue
            if for_flag:                    # la aparitia instructiunii for
                if token.type == TokenTypePseudocode.NEWLINE:   # se reseteaza flagul la linie noua
                    indentation += "\t"                         # se parseaza instructiunea
                    output += Translator.parse_pseudo_for(for_data) + "\n" + indentation[:-1] + "{\n" + indentation
                    for_data = ""
                    for_flag = False
                else:
                    for_data += token.value + " "
                continue
            if dqmarks and token.type != TokenTypePseudocode.DQMARK:    # in cazul incluziunii unui string, se introduc datele integral
                output += token.value
                continue
            if token.type == TokenTypePseudocode.DQMARK:                # incluziune string
                if dqmarks:
                    dqmarks = False
                else:
                    dqmarks = True
                output += token.value
            elif token.type == TokenTypePseudocode.COMMENT:             # comentariu pe o singura linie, se seteaza flagul
                output += token.value + " "
                comment = True
            elif token.type == TokenTypePseudocode.OPENCOMMENT:         # comentariu multiplu, se seteaza flagul
                output += token.value + " "
                multiple_comment = True
            elif token.type == TokenTypePseudocode.THEN:                # token THEN
                if paren_opened:                                        # se inchide paranteza in cazul deschiderii
                    output += ")\n" + indentation
                    paren_opened = False
                indentation += "\t"
                output += "{\n" + indentation                           # se deschide un bloc prin acolada
            elif token.type == TokenTypePseudocode.END:                 # token END
                indentation = indentation[:-1]
                if output[-1] == '\t':
                    output = output[:-1]
                output += "}"                                           # se inchide un bloc
                last_line = token
            elif token.type == TokenTypePseudocode.NEWLINE:             # linie noua
                if until_flag:                                          # caz instructiune repeat until
                    if paren_opened:                                    # daca a fost necesara includerea unei noi paranteze
                        output += "));"                                 # se inchide aceasta
                        paren_opened = False
                    else:                                               # se inchide instructiunea do ... while(...);
                        output += ");"
                    until_flag = False
                else:
                    if paren_opened:
                        output += ")"
                        indentation += "\t"
                        paren_opened = False
                    else:
                        # fiecare instructiune in pseudocod este scrisa pe o linie separata
                        # prin urmare, ; se va pune in concordanta cu liniile noi
                        # nu se pune ; dupa AFTER_INSTRUCTIONS si nici inainte de BEFORE_INSTRUCTIONS
                        index_token = tokens.index(token)
                        if index_token - 1 > 0:
                            if tokens[index_token - 1].value not in Instructions.AFTER_INSTRUCTIONS:
                                if index_token + 1 < len(tokens):
                                    if tokens[index_token + 1].value not in Instructions.BEFORE_INSTRUCTIONS and\
                                       tokens[index_token + 1].type != TokenTypePseudocode.NEWLINE:
                                        output += ";"
                                    else:
                                        if last_line.type == "NAME":
                                            output += ";"
                output += token.value + indentation
            elif token.type == TokenTypePseudocode.IF:  # token IF
                index_token = tokens.index(token)
                if index_token + 1 < len(tokens):   # fortarea parantezei in cazul in care nu apare
                    if tokens[index_token + 1].type == TokenTypePseudocode.LPAREN:
                        output += "if"
                    else:
                        output += "if ("
                        paren_opened = True
                last_line = token
            elif token.type == TokenTypePseudocode.WHILE:   # token while
                index_token = tokens.index(token)
                if index_token + 1 < len(tokens):
                    if tokens[index_token + 1].type == TokenTypePseudocode.LPAREN:
                        output += "while"
                    else:
                        output += "while ("
                        paren_opened = True
                last_line = token
            elif token.type == TokenTypePseudocode.REPEAT:  # token repeat
                output += "do{"
                indentation += "\t"
                last_line = token
            elif token.type == TokenTypePseudocode.UNTIL:   # token until
                index_token = tokens.index(token)
                if index_token + 1 < len(tokens):
                    if tokens[index_token + 1].type == TokenTypePseudocode.LPAREN:
                        if tokens[index_token - 1].type == TokenTypePseudocode.NEWLINE:
                            output = output[:-1]
                        indentation = indentation[:-1]
                        output += "}while(!"
                    else:
                        if tokens[index_token - 1].type == TokenTypePseudocode.NEWLINE:
                            output = output[:-1]
                        indentation = indentation[:-1]
                        output += "}while(!("
                        paren_opened = True
                        until_flag = True
                    last_line = token
            elif token.type == TokenTypePseudocode.ELSE:    # token else
                indentation = indentation[:-1]
                if output[-1] == "\t":
                    output = output[:-1]
                output += "}\n"+indentation+"else\n"+indentation+"{"
                indentation += "\t"
                last_line = token
            elif token.type == TokenTypePseudocode.FOR:     # token for, se seteaza flagul
                for_flag = True
                for_data = "for "
                last_line = token
            elif token.type == 'NAME':                      # variabila sau nume de functie
                index_token = tokens.index(token)
                if index_token + 1 < len(tokens):
                    if tokens[index_token + 1].type == 'NAME':
                        output += token.value + " "
                    else:
                        output += token.value
                else:
                    output += token.value
                last_line = token
            elif token.type == TokenTypePseudocode.BEGIN:   # begin
                output += "#include <stdio.h>\n\nint main()\n{\n\t"
                if variables:
                    tps = dict()
                    # adaugarea definirii variabilelor din dictionarul variables
                    for var in list(variables.keys()):
                        if variables[var]["type"] in list(tps.keys()):
                            if variables[var]["ndims"] == 1:
                                varadd = var + "[" + str(variables[var]["dims"][0]) + "]"
                            elif variables[var]["ndims"] == 2:
                                varadd = var + "[" + str(variables[var]["dims"][0]) + "][" + str(variables[var]["dims"][1]) + "]"
                            else:
                                varadd = var
                            tps[variables[var]["type"]].append(varadd)
                        else:
                            tps[variables[var]["type"]] = list()
                            if variables[var]["ndims"] == 1:
                                varadd = var + "[" + str(variables[var]["dims"][0]) + "]"
                            elif variables[var]["ndims"] == 2:
                                varadd = var + "[" + str(variables[var]["dims"][0]) + "][" + str(variables[var]["dims"][1]) + "]"
                            else:
                                varadd = var
                            tps[variables[var]["type"]].append(varadd)
                    for tp in tps:
                        output += tp + " "
                        for var in tps[tp]:
                            output += var+", "
                        output = output[:-2] + ";\n\t"
                indentation = "\t"
                last_line = token
            elif token.type == TokenTypePseudocode.EQUAL:
                output += " == "
            elif token.type == TokenTypePseudocode.ATRIB:
                output += " = "
            elif token.type == TokenTypePseudocode.TRUE:
                output += "true"
            elif token.type == TokenTypePseudocode.FALSE:
                output += "false"
            elif token.type == TokenTypePseudocode.READ:
                read_flag = True
                read_data = ""
            elif token.type == TokenTypePseudocode.PRINT:
                print_flag = True
                print_data = ""
            elif token.type == TokenTypePseudocode.AND:
                output += " && "
            elif token.type == TokenTypePseudocode.OR:
                output += " || "
            else:
                output += token.value
        output = output[:-1]
        output += "\treturn 0;\n}"
        return output

    def c_to_py(self):
        # definirea regulilor pentru translatarea in Python din C
        output = ""  # setarea outputului ca string gol
        # se realizeaza tokenizarea
        self.tokenizer.tokenize_c()
        # se obtine lista de tokeni
        tokens = self.tokenizer.token_list
        single_comment = False              # flag pentru inceput de comentariu pe o linie
        multiple_comment = False            # flag pentru inceput de comentariu pe mai multe linii
        indentation = ''                    # variabila pentru identare
        open_array = 0                      # flag care numara cate array-uri s-au gasit
        function_definition = False         # flag pentru definirea functiilor
        include_flag = False                # flag pentru importarea pachetelor
        pack = ""                           # variabila pentru importarea pachetelor
        define_flag = False                 # flag pentru definirea constantelor
        variable_definition = False         # flag pentru definirea variabilelor
        dqmarks = False                     # flag care specifica daca datele scrise apartin unui sir de caractere
        array_sized = False                 # flag pentru stergerea dimensiunii unui array la definire
        condition_flag = False              # flag pentru adaugarea caracterului : dupa if sau while
        redundant_paren = False             # flag pentru scoaterea parantezelor redundante
        function_occurrence = False         # flag pentru functiile definite
        function_data = ""                  # inputul pentru metoda de parsare de functii
        for_occurrence = False              # flag pentru parsare for
        for_data = ""                       # datele din for ce trebuie parsate
        increment_flag = False              # flag pentru scrierea incrementarilor si decrementarilor
        dimensions = list()                 # lista folosita la definirea matricelor
        last_type = ""                      # tipul de variabila definit ultima dată
        for token in tokens:                # se parcurg toti tokenii gasiti
            if function_occurrence:
                if token.type != TokenType.RPAREN:
                    function_data += token.value
                    continue
                else:
                    function_occurrence = False
                    function_data += token.value
                    to_add = Translator.parse_c_function(function_data, indentation)
                    output += to_add
                    function_data = ""
                    continue
            if for_occurrence:
                if token.type != TokenType.RPAREN:
                    for_data += token.value
                    continue
                else:
                    for_occurrence = False
                    for_data += token.value
                    to_add = Translator.process_for(for_data)
                    output += to_add
                    for_data = ""
                    continue
            if dqmarks and token.type != TokenType.DQMARK:
                output += token.value
                continue
            if single_comment:                      # comentariu pe linie inceput
                if token.type == TokenType.NEWLINE:
                    output += "\n" + indentation    # se adauga linia noua plus identarea pe linia urmatoare
                    single_comment = False          # comentariul s-a terminat
                else:                               # orice alt token este scris asa cum a fost scris
                    output += token.value + " "     # se adauga valoarea si un spatiu
            if multiple_comment:                    # comentariu multiplu initiat
                if token.type == TokenType.NEWLINE:         # la linie noua
                    output += "\n" + indentation + " # "    # se incepe o noua linie de comentariu
                elif token.type == TokenType.CLOSECOMMENT:  # se inchide comentariul
                    output += "\n" + indentation
                    multiple_comment = False
                    continue
                else:                               # orice alt token este adaugat integral
                    output += token.value + " "
            if not single_comment and not multiple_comment:  # nu s-a inceput un comentariu
                if token.type == TokenType.COMMENT:
                    single_comment = True           # se activeaza flagul pentru comentariu pe o singura linie
                    output += " # "                 # se adauga caracterul pentru comentariu
                elif token.type == TokenType.OPENCOMMENT:   # token pentru comentariu pe mai multe linii
                    multiple_comment = True
                    output += " # "                 # se adauga caracterul pentru comentariu
                elif token.type == TokenType.NEWLINE:       # adaugare linie noua
                    index_token = tokens.index(token)
                    if condition_flag:              # daca este linie pentru if sau while
                        if redundant_paren:         # s-a scos LPAREN deci trebuie scoasa RPAREN
                            output = output[:-1]
                            redundant_paren = False
                        output += ":"
                        condition_flag = False
                    if index_token > 0:
                        if tokens[index_token - 1].type != TokenType.LCBRACE and \
                                tokens[index_token - 1].type != TokenType.RCBRACE:
                            output += "\n" + indentation
                elif token.type == TokenType.LCBRACE:   # pentru { se adauga un \t la indentare
                    index_token = tokens.index(token)
                    if tokens[index_token - 1].type == TokenType.ATRIB or open_array:
                        # atribuire array sau matrice
                        output += '['
                        open_array += 1             # creste numarul de array-uri deschise
                    else:                           # { delimiteaza un bloc
                        if tokens[index_token - 1].type != TokenType.NEWLINE:   # { nu a fost pus pe linie noua
                            if condition_flag:
                                if redundant_paren:     # scoatere ) inutila
                                    output = output[:-1]
                                    redundant_paren = False
                                output += ":"
                                condition_flag = False
                            output += "\n" + indentation    # se introduce o linie noua pentru aliniere
                        indentation += "\t"
                        output += "\t"              # se adauga un \t si la output pentru aliniere
                elif token.type == TokenType.RCBRACE:       # pentru } se sterge un \t din indentare
                    if open_array:                  # exista array-uri deschise
                        output += ']'               # se inchide lista echivalenta
                        open_array -= 1             # se decrementeaza numarul de array-uri deschise
                    else:
                        index_token = tokens.index(token)
                        if tokens[index_token - 1].type != TokenType.NEWLINE:  # } nu a fost pus pe linie noua
                            output += "\n" + indentation    # se introduce o linie noua pentru aliniere
                        indentation = indentation[:-1]
                        output = output[:-1]        # se scoate un \t din output pentru aliniere
                elif token.type == TokenType.AMPERSAND:
                    # ampersandul poate fi folosit ca and pe biti sau pentru a referi adresa unei variabile
                    index_token = tokens.index(token)
                    if index_token > 0:
                        if tokens[index_token - 1].type != 'NAME':  # nu este o variabila inainte
                            # adresa
                            # in python, nu sunt referite adrese si nu exista pointeri, caracterul este ignorat
                            pass
                        else:
                            # bitwise and
                            output += ' &'
                    else:
                        output += ' &'
                elif token.type == TokenType.SEMICOLON:
                    if variable_definition:
                        variable_definition = False
                    index_token = tokens.index(token)
                    if len(tokens) > index_token + 1:
                        if tokens[index_token + 1].type != TokenType.NEWLINE:
                            # daca nu se afla inaintea unui newline, se forteaza un newline
                            output += '\n' + indentation
                elif token.type == TokenType.INTEGER or token.type == TokenType.FLOAT or \
                        token.type == TokenType.DOUBLE or token.type == TokenType.BOOL or token.type == TokenType.CHAR:
                    # tipul de data poate aparea pentru definirea unei variabile sau a unei functii
                    last_type = token.type
                    index_token = tokens.index(token)
                    if index_token + 2 < len(tokens):
                        if tokens[index_token + 2].type == TokenType.LPAREN:
                            # daca al doilea token dupa cel curent este o paranteza deschisa
                            # atunci tokenul curent este folosit pentru definirea unei functii
                            output += 'def'
                            function_definition = True
                        else:
                            # in caz contrar, tipul de date nu este mentionat
                            # dar se seteaza flagul de definire de variabile
                            variable_definition = True
                elif token.type == TokenType.RPAREN:
                    # paranteza inchisa poate insemna terminarea definirii unei functii
                    if function_definition:
                        # se adauga pe langa paranteza si :
                        output += '):'
                        function_definition = False
                        variable_definition = False
                    else:
                        output += token.value
                elif token.type == TokenType.INCLUDE:
                    # importarea unui pachet
                    include_flag = True
                    pack = ""
                elif token.type == TokenType.LT:
                    if not include_flag:
                        # nu se include un pachet deci < este doar lower than
                        output += ' < '
                    # in caz contrar, caracterul nu va fi inclus
                elif token.type == TokenType.GT:
                    if include_flag:
                        # include flag setat, atunci trebuie adaugat echivalentul pachetului retinut
                        output += PackageEquivalent.Packages[pack]
                        include_flag = False
                    else:
                        output += ' > '
                elif token.type == "NAME":
                    if include_flag:
                        # daca se include un pachet, tokenul va fi adaugat la numele pachetului
                        pack += token.value
                    else:
                        index_token = tokens.index(token)
                        if index_token + 1 < len(tokens):
                            if tokens[index_token + 1].type == TokenType.LPAREN:
                                if not function_definition:
                                    function_occurrence = True
                                    function_data += token.value
                                    continue
                        # se adauga valoarea tokenului
                        if variable_definition:
                            if tokens[index_token + 1].type != TokenType.ATRIB \
                                    and tokens[index_token + 1].type != TokenType.LBRACE:
                                if function_definition:
                                    output += " " + token.value
                            else:
                                output += " " + token.value
                        else:
                            output += " " + token.value

                        if define_flag:
                            # se adauga si un egal deoarece urmeaza valoarea constantei
                            output += " = "
                            define_flag = False
                        if increment_flag:
                            output += increment_type
                            increment_flag = False
                elif token.type == TokenType.DOT:
                    if include_flag:
                        pack += token.value
                    # else:
                    #     output += token.value
                elif token.type == TokenType.COMMA:
                    if function_definition:
                        output += token.value
                        continue
                    if variable_definition and open_array == 0:
                        output += '\n' + indentation
                    else:
                        output += token.value
                elif token.type == TokenType.DEFINE:
                    define_flag = True
                elif token.type == TokenType.MUL:
                    # * poate fi folosita pentru pointeri
                    index_token = tokens.index(token)
                    if index_token > 0:
                        # este inmultire doar daca inainte exista o variabila, un numar sau o paranteza inchisa
                        if tokens[index_token - 1].type == 'NAME' or \
                                tokens[index_token - 1].type == TokenType.RPAREN or \
                                tokens[index_token - 1].type == 'NUMERIC':
                            output += " *"
                        # in caz contrar, * se foloseste pentru pointeri si este ignorata
                elif token.type == TokenType.LBRACE:
                    if not variable_definition:
                        output += '['
                    else:
                        array_sized = True
                elif token.type == TokenType.RBRACE:
                    if not variable_definition:
                        output += ']'
                        if increment_flag:
                            output += increment_type
                            increment_flag = False
                    else:
                        array_sized = False
                elif token.type == TokenType.DQMARK:
                    if dqmarks is True:
                        dqmarks = False
                    else:
                        dqmarks = True
                    output += '"'
                elif token.type == TokenType.LPAREN:
                    if not condition_flag or redundant_paren:
                        # daca nu este activat flagul de conditie sau s-a scos paranteza redundanta
                        # se adauga valoarea tokenului
                        output += token.value
                    else:
                        # altfel nu se adauga valoarea tokenului ceea ce inseamna ca s-a scos paranteza
                        # si se seteaza flagul
                        redundant_paren = True
                elif token.type == TokenType.INCREMENT:
                    index_token = tokens.index(token)
                    if index_token >= 1:
                        if tokens[index_token - 1].type == 'NAME' or tokens[index_token - 1].type == TokenType.RBRACE:
                            output += " += 1"
                        else:
                            increment_flag = True
                            increment_type = " += 1"
                    else:
                        increment_flag = True
                        increment_type = " += 1"
                elif token.type == TokenType.DECREMENT:
                    index_token = tokens.index(token)
                    if index_token >= 1:
                        if tokens[index_token - 1].type == 'NAME' or tokens[index_token - 1].type == TokenType.RBRACE:
                            output += " -= 1"
                        else:
                            increment_flag = True
                            increment_type = " -= 1"
                    else:
                        increment_flag = True
                        increment_type = " -= 1"
                elif token.type == TokenType.AND:
                    output += ' and '
                elif token.type == TokenType.OR:
                    output += ' or '
                elif token.type == TokenType.TRUE:
                    output += ' True '
                elif token.type == TokenType.FALSE:
                    output += ' False '
                elif token.type == TokenType.ELSE:
                    output += ' ' + token.value + ":"
                elif token.type == TokenType.IF or token.type == TokenType.WHILE:
                    condition_flag = True
                    output += " " + token.value + " "
                elif token.type == TokenType.FOR:
                    for_occurrence = True
                elif token.type == 'NUMERIC':
                    if array_sized:
                        index_token = tokens.index(token)
                        if index_token + 3 < len(tokens):
                            if tokens[index_token + 3].type != TokenType.LCBRACE:
                                if tokens[index_token + 2].type == TokenType.LBRACE:
                                    dimensions.append(token.value)
                                else:
                                    output += " = "
                                    if last_type == TokenType.INTEGER or last_type == TokenType.FLOAT or last_type == TokenType.DOUBLE:
                                        aux = "[0"
                                    elif last_type == TokenType.BOOL:
                                        aux = "[True"
                                    else:
                                        aux = "[\"\""
                                    indexdim = 0
                                    dimensions.append(token.value)
                                    dimensions.reverse()
                                    for dim in dimensions:
                                        aux += " for dim" + str(indexdim) + " in range("+dim+")]"
                                        indexdim += 1
                                    for i in range(indexdim -1):
                                        aux = "[" + aux
                                    output += aux
                                    dimensions = list()
                    else:
                        output += token.value
                else:
                    output += " " + token.value + " "
        if "def main():" in output:
            output = output.replace("def main():", "if __name__ == \"__main__\":")
            output = output.replace("return 0", "")
        output = output.replace('\t', '    ')
        return output

    def translate(self):
        output = ""
        if self.fromlang == "C":                # limbajul in care este scris este C
            if self.tolang == "Python":         # in python
                output = self.c_to_py()
        elif self.fromlang == "PSEUDOCOD":      # din pseudocod
            if self.tolang == "C":              # in C
                output = self.pseudo_to_c()
        self.output = output
        return output

    def eliminate_spaces(self):                 # eliminarea liniilor goale
        text = self.output.splitlines()
        out = list()
        for line in text:
            if not line.isspace():
                out.append(line)
        self.output = "\n".join(out)

    @staticmethod
    def parse_conditie(conditie):               # parsarea instructiunilor conditionale
        if "<=" in conditie:
            conditie = conditie.split("<=")
            return str(int(conditie[1]) + 1)
        if ">=" in conditie:
            conditie = conditie.split(">=")
            return str(int(conditie[1]) - 1)
        if ">" in conditie:
            conditie = conditie.split(">")
            return conditie[1]
        if "<" in conditie:
            conditie = conditie.split("<")
            return conditie[1]

    @staticmethod
    def parse_continuare(continuare):               # modul de incrementare al unei instructiuni for
        if "++" in continuare:
            return "1"
        if "--" in continuare:
            return "-1"
        if "+=" in continuare:
            continuare = continuare.split("+=")
            return continuare[1]
        if "-=" in continuare:
            continuare = continuare.split("-=")
            return "-"+continuare[1]
        if "+" in continuare:
            continuare = continuare.split("+")
            ret = "0"
            for element in continuare:
                if element.isnumeric():
                    ret = element
                    break
            return ret
        if "-" in continuare:
            continuare = continuare.split("-")
            ret = "0"
            for element in continuare:
                if element.isnumeric():
                    ret = element
                    break
                return "-" + ret

    @staticmethod
    def parse_pseudo_for(for_data):                 # parsarea instructiunii for pentru pseudocod
        for_data = for_data.replace('for', '', 1)
        if for_data.startswith('('):
            for_data = for_data.replace('(', '', 1)
            for_data = for_data[::-1]
            for_data = for_data.replace(')', '', 1)
            for_data = for_data[::-1]
        var = ""
        initial = ""
        final = ""
        step = ""
        sign = ""
        if "TO" in for_data:
            splat = for_data.split('TO')
            initial = splat[0].split('=')[1]
            var = splat[0].split('=')[0]
            if "STEP" in splat[1]:
                step = splat[1].split("step")[1]
                final = splat[1].split("step")[0]
                if int(initial) > int(final):
                    sign = ">="
                else:
                    sign = "<="
            else:
                final = splat[1]
                if int(initial) > int(final):
                    step = "-1"
                    sign = ">="
                else:
                    step = "1"
                    sign = "<="
        var = var.replace(" ", "")
        initial = initial.replace(" ", "")
        final = final.replace(" ", "")
        step = step.replace(" ", "")
        ret = "for ("+var+"="+initial+";"+var+sign+final+";"+var
        if step == "1":
            ret += "++)"
        elif step == "-1":
            ret += "--)"
        elif '-' in step:
            ret += "-=" + step.replace('-', '') + ')'
        else:
            ret += "+=" + step + ")"
        return ret

    def parse_read_pseudocode(self, read_data, indent):         # parsarea instructiunii read in pseudocod
        output = indent
        read_data = read_data.split(",")
        for var in read_data:
            if "[" in var:
                idx = var.index("[")
            else:
                idx = None      # daca upper value e None, se ia stringul integral
            # se separa dupa tipul variabilei apoi dupa numar de dimensiuni
            if self.variables[var[0:idx]]["type"] == "char":
                if idx is not None:     # ne spune daca exista [ in numele variabilei
                    if self.variables[var[0:idx]]["ndims"] == 1:
                        if self.pseudo_read_index != 0:
                            output += "getchar(); // golire spatiu din bufferul de intrare\n"
                        output += indent + var + " = getchar();\n" + indent
                    elif self.variables[var[0:idx]]["ndims"] == 2:
                        output += "scanf(\"%s\","+var+");\n"+indent
                else:
                    if self.variables[var]["ndims"] == 0:
                        if self.pseudo_read_index != 0:
                            output += "getchar(); // golire spatiu din bufferul de intrare\n"
                        output += indent + var + " = getchar();\n" + indent
                    elif self.variables[var]["ndims"] == 1:
                        output += "scanf(\"%s\"," + var + ");\n" + indent
                    else:   # 2
                        output += "for (int i_"+var+"=0;i_"+var+"<"+str(self.variables[var]["dims"][0])+";i_"+var+"++)\n"+indent + "\t"
                        output += "scanf(\"%s\"," + var + "[i_"+var+"]);\n" + indent
            else:
                if self.variables[var[0:idx]]["ndims"] == 0:
                    output += "scanf(\""+FunctionUtil.PSEUDO_READ_FORMATERS[self.variables[var[0:idx]]["type"]]+"\", &"+var+");\n"+indent
                elif self.variables[var[0:idx]]["ndims"] == 1:
                    if idx is not None:
                        output += "scanf(\"" + FunctionUtil.PSEUDO_READ_FORMATERS[
                            self.variables[var[0:idx]]["type"]] + "\", &" + var + ");\n" + indent
                    else:
                        output += "for (int i_"+var[0:idx]+"=0;i_"+var[0:idx]+"<"+str(self.variables[var[0:idx]]["dims"][0])+";i_" + \
                            var[0:idx] + "++)\n"+indent+"\t"
                        output += "scanf(\"" + FunctionUtil.PSEUDO_READ_FORMATERS[
                            self.variables[var[0:idx]]["type"]] + "\", &" + var[0:idx] + "[i_"+var[0:idx]+"]);\n" + indent
                elif self.variables[var[0:idx]]["ndims"] == 2:
                    if var.count('[') == 1:
                        output += "for (int i_" + var[0:idx] + "=0;i_" + var[0:idx] + "<" + \
                                  str(self.variables[var[0:idx]]["dims"][1]) + ";i_" + \
                                  var[0:idx] + "++)\n" + indent + "\t"
                        output += "scanf(\"" + FunctionUtil.PSEUDO_READ_FORMATERS[
                            self.variables[var[0:idx]]["type"]] + "\", &" + var + "[i_" + var[0:idx] + "]);\n" + indent
                    elif var.count('[') == 0:
                        output += "for (int i_" + var[0:idx] + "=0;i_" + var[0:idx] + "<" + \
                                  str(self.variables[var[0:idx]]["dims"][0]) + ";i_" + \
                                  var[0:idx] + "++)\n" + indent + "\t"
                        output += "for (int j_" + var[0:idx] + "=0;j_" + var[0:idx] + "<" + \
                                  str(self.variables[var[0:idx]]["dims"][1]) + ";j_" + \
                                  var[0:idx] + "++)\n" + indent + "\t\t"
                        output += "scanf(\"" + FunctionUtil.PSEUDO_READ_FORMATERS[
                            self.variables[var[0:idx]]["type"]] + "\", &" + var + "[i_" + var[0:idx] + "][j_" + var[0:idx] +\
                            "]);\n" + indent
                    else:   # count 2
                        output += "scanf(\"" + FunctionUtil.PSEUDO_READ_FORMATERS[
                            self.variables[var[0:idx]]["type"]] + "\", &" + var + ");\n" + indent
            self.pseudo_read_index += 1
        return output

    def parse_print_pseudocode(self, print_data, indentation):          # parsarea instructiunii print pentru pseudocod
        if '(' in print_data:
            print_data.replace("(", "", 1)
            print_data = print_data[::-1]
            print_data.replace(")", "", 1)
            print_data = print_data[::-1]
        print_data = print_data.split(',')
        output = indentation + "printf(\""
        to_add = ""
        if len(print_data) > 1 or print_data[0] != "":
            to_add += ", "
        for var in print_data:
            if var in self.variables:
                output += FunctionUtil.PSEUDO_READ_FORMATERS[self.variables[var]["type"]] + " "
                to_add += var + ", "
            else:
                if '"' in var:
                    var = var.replace('"', '')
                    output += var
        to_add = to_add[:-2]
        output += '"' + to_add + ');\n'+indentation
        return output


# pentru test
if __name__ == "__main__":
    # a = """
    #             #include <stdio.h>
    #             int main() {
    #                 int number1, number2, sum;
    #                 int a[] = {0,1,2,3}, b[10];
    #                 int c[2];
    #                 int d[][3] = {{1,2,3}, {4,5,6}};
    #                 int e[2][10];
    #                 int p = 0;
    #                 p++;
    #                 ++p;
    #                 printf("Enter two integers: ");
    #                 scanf("%d %d", &number1, &number2);
    #                 // calculating sum
    #                 /* unu
    #                 doi
    #                 trei */
    #                 sum = number1 + number2;
    #                 if ((2 > 1) && (1 < 2))
    #                 {
    #                     printf(true);
    #                 }
    #                 while (1 > 2)
    #                 {
    #                     a[1] = 0
    #                 }
    #                 a[2]--;
    #                 printf("%d + %d = %d", number1, number2, sum);
    #                 int i;
    #                 for (i = 10; i >= 0; i--)
    #                 {
    #                     printf("Hello");
    #                 }
    #                 return 0;
    #             }"""
    variables = {
        'a': {"type": 'int', "ndims": 0},
        'b': {"type": 'int', "ndims": 0},
        'i': {"type": 'int', "ndims": 0},
        'c': {"type": 'int', "ndims": 1, "dims": [4]},
        'd': {"type": 'int', "ndims": 2, "dims": [4, 2]},
        'e': {"type": 'char', "ndims": 0},
        'f': {"type": 'char', "ndims": 1, "dims": [4]},
        'g': {"type": 'char', "ndims": 2, "dims": [4, 2]}
    }
    a = """
        BEGIN
            FOR X = 1 TO 10
                FOR Y = 1 TO 10
                    IF gameBoard[X][Y] = 0 THEN
                        // Do nothing
                    ELSE
                        theCall(X, Y)
                        counter++                 
                    END
                END
            END
            /* ana are mere
            ana are mere*/
            WHILE X != Y THEN
                a <- a + 2
                b <- b - a
            END
            READ a,b,i
            PRINT "a = ",a,"b = ", b
            i <- 0
            b <- 3
            REPEAT
                i <- i+2
            UNTIL i = 6 AND b = 3
            READ c
            READ d
            READ c[1]
            READ d[2]
            READ d[1][2]
            READ e
            READ f
            READ g
        END"""
    t = Translator(a, 'PSEUDOCOD', 'C', variables)
    o = t.translate()
    t.eliminate_spaces()
    print(t.output)


